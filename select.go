package main

import (
	"fmt"
	"runtime"
)

func getAverage(numbers []int, result chan float64){
	var sum int
	for _, e := range numbers{
		sum += e
	}
	result <- float64(sum) / float64(len(numbers))
}

func getMin(numbers []int, result chan int){
	var min = numbers[0]
	for _, e := range numbers{
		if min > e {
			min = e
		}
	}
	result <- min
}

func getMax(numbers []int, result chan int){
	var max = numbers[0]
	for _, e := range numbers{
		if max < e {
			max = e
		}
	}
	result <- max
}

//func getSumMinMax(min chan int, max chan int, result chan int){
//	var minSum int = <-min
//	var maxSum int = <-max
//	result <- minSum + maxSum
//}

func main() {
	runtime.GOMAXPROCS(2)
	var numbers = []int{3, 4, 3, 5, 6, 3, 2, 2, 6, 5, 4, 6, 15, 24, 55, 33, 1, 5, 9, 10}

	var channelAverage = make(chan float64)
	var channelMin = make(chan int)
	var channelMax = make(chan int)
	//var channelSumMinMax = make(chan int)

	go getAverage(numbers, channelAverage)
	go getMin(numbers, channelMin)
	go getMax(numbers, channelMax)
	//go getSumMinMax(channelMin, channelMax, channelSumMinMax)
	//avg1 := <-channelAverage
	//avg2 := avg1<--
	//fmt.Printf("Average1 \t: %.2f \n", avg1)
	for i := 0; i <= 2; i++ {
		select {
		case avg := <-channelAverage:
			fmt.Printf("Average \t: %.2f \n", avg)
		case min := <-channelMin:
			fmt.Printf("Min \t: %d \n", min)
		case max := <-channelMax:
			fmt.Printf("Max \t: %d \n", max)
		//case sum := <-channelSumMinMax:
		//	fmt.Printf("Sum \t: %d \n", sum)
		}
	}
}
