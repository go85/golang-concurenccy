package main

import (
	"fmt"
	"runtime"
)

func printMessage(say chan string){
	fmt.Println(<-say)
}

func main() {
	runtime.GOMAXPROCS(2)
	var messages = make(chan string)
	var messagesLoop = make(chan string)

	var sayHelloTo = func(word string){
		var data, _ = fmt.Printf("holla %s \n", word)
		messages <- string(data)
	}

	go sayHelloTo("nodejs")
	go sayHelloTo("python")
	go sayHelloTo("php")

	var message1 = <- messages
	var message2 = <- messages
	var message3 = <- messages
	fmt.Println(message1)
	fmt.Println(message2)
	fmt.Println(message3)

	//for _, each := range []string{"wahyu", "ariel", "enma"}{
	//	go func(who string){
	//		var data = fmt.Sprintf("halo %s", who)
	//		messagesLoop <- data
	//	}(each)
	//}

	//for i := 0; i < 3; i++ {
	//	printMessage(messagesLoop)
	//}

	go func(who string){
		var data = fmt.Sprintf("halo %s", who)
		messagesLoop <- data
	}("august")

	printMessage(messagesLoop)

}
