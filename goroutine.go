package main

import (
	"fmt"
	"runtime"
)

func print(till int, word string){
	for i := 0; i < till; i ++ {
		fmt.Printf("%d. %s",i, word)
		fmt.Println()
	}
}

func main() {
	//use to determine the number of cores enabled for program execution
	runtime.GOMAXPROCS(2)

	go print(5, "hello world")
	print(5, "hello too")

	var input string
	fmt.Scanln(&input)
}
