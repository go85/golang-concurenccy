package main

import (
	"fmt"
	"runtime"
)

func sendMessage(msg chan<- string){
	for i := 0; i < 20; i++ {
		msg <- fmt.Sprintf("data %d", i)
	}
	close(msg)
}

func receiveMessage(msg <-chan string){
	for message := range msg{
		fmt.Println(message)
	}
}

func main() {
	runtime.GOMAXPROCS(2)
	var message = make(chan string)
	go sendMessage(message)
	receiveMessage(message)
}
