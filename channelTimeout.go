package main

import (
	"fmt"
	"math/rand"
	"runtime"
	"time"
)

func sendData(data chan<- int){
	for i := 0; true; i++ {
		data <- i
		time.Sleep(time.Duration(rand.Int()%10+1) * time.Second)
	}
}

func receiveData(data <-chan int){
	loop:
	for {
		select {
		case number := <-data:
			fmt.Print(`receive Data "`, number, `"`, "\n")
		case <-time.After(time.Second * 5):
			fmt.Println("timeout. no activities under 5 seconds")
			break loop
		}
	}
}

func main() {
	rand.Seed(time.Now().Unix())
	runtime.GOMAXPROCS(2)

	var message = make(chan int)
	go sendData(message)
	receiveData(message)
}
