package main

import (
	"fmt"
	"sync"
)

func doPrint(wg *sync.WaitGroup, message string){
	fmt.Println(message)
	defer wg.Done()
}

func main() {
	var wg sync.WaitGroup

	for i := 1; i <= 15; i++ {
		var data = fmt.Sprintf("data %d", i)

		wg.Add(1)
		go doPrint(&wg, data)
	}

	wg.Wait()
}
